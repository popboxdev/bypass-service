const sqlite3 = require('sqlite3').verbose();

// open the database
const db = new sqlite3.Database('D:/popboxclient_ina/database/popboxclient.db', sqlite3.OPEN_READWRITE, (err) => {
    if (err) {
      console.error(err.message);
    }
    console.log('Connected to the popboxclient database.');
});

const expressData = function (sql, callback) {
    db.all(sql, (err, row) => {
        if (err) {
            console.error(err.message);
        }
        callback(row);
    }); 
}

module.exports = {
    expressData
};