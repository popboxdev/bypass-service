module.exports = {
    server : {
        host: '0.0.0.0',
        port: 8000
    },
    orm : {
        db: {
            clients: 'sqlite3',
            database: 'popboxclient',
        }
    }
}